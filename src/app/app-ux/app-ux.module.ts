import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {InputRowComponent} from './input-row/input-row.component';
import {FormComponent} from './form/form.component';
import {ListInputComponent} from './list-input/list-input.component';
import {AutocompleteComponent} from './autocomplete/autocomplete.component';
import { TableComponent } from './table/table.component';
import { ModalComponent } from './modal/modal.component';
import {ModalService} from './modal/modal.service';
import {ColorPickerModule} from 'angular4-color-picker';

@NgModule({
    imports: [
        RouterModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        ColorPickerModule
    ],
    declarations: [
        FormComponent,
        InputRowComponent,
        ListInputComponent,
        AutocompleteComponent,
        TableComponent,
        ModalComponent
    ],
    exports: [
        FormComponent,
        InputRowComponent,
        ListInputComponent,
        AutocompleteComponent,
        TableComponent,
        ModalComponent
    ],
    providers: [
        ModalService
    ]
})
export class AppUxModule {
}
