import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnChanges, OnInit {
    @Input() data: Array<any> = [];
    @Input() header: any;

    showoffData: Array<Array<string>> = [];
    fields: Array<string> = [];
    headerFields: Array<string> = [];

    ngOnChanges() {
        this.prepare();
    }

    ngOnInit() {
        this.prepareHeader();
        this.prepare();
    }

    private prepareHeader() {
        this.headerFields = Object.keys(this.header).map(key => this.header[key].fieldName);
        this.fields = Object.keys(this.header);
    }

    private prepare() {
        this.showoffData = [];
        for (const element of this.data) {
            this.showoffData.push(this.prepareRow(element));
        }
    }

    private prepareRow(row) {
        const array = [];
        for (const element of this.fields) {
            let value = this.findField(element, row);
            if (this.header[element].callback !== undefined) {
                value = this.header[element].callback(value);
            }
            array.push(value.toString());
        }

        return array;
    }

    private findField(path, row) {
        const pathSteps = path.split('.');
        let value = row;

        for (const step of pathSteps) {
            if (row.hasOwnProperty(step)) {
                value = value[step];
            } else {
                return undefined;
            }
        }

        return value;
    }
}
