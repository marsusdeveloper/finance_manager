import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {InputModel, ValidationModel} from '../app-ux-model';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-ux-form',
    templateUrl: './form.component.html',
    styleUrls: ['form.component.scss']
})

export class FormComponent implements OnChanges {
    @Input() list: InputModel[];
    @Output() formUpdate = new EventEmitter();
    form = new FormGroup({});

    ngOnChanges() {
        if (this.list) {
            this.prepareFormGroup();
        }
    }

    submitForm(event) {
        event.preventDefault();
        this.formUpdate.emit(this.form);
    }

    private prepareFormGroup() {
        const formControlObjectsCollection = {};

        for (const listElement of this.list) {
            if (!listElement.validation) {
                listElement.validation = ValidationModel;
            }
            formControlObjectsCollection[listElement.id] = new FormControl(listElement.value, listElement.validation);
            listElement.formControl = formControlObjectsCollection[listElement.id];
        }

        this.form = new FormGroup(formControlObjectsCollection);
    }
}
