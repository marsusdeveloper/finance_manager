import {Injectable} from '@angular/core';

@Injectable()
export class ModalService {
    private modals = {
        'default': false
    };

    constructor() {
    }

    open(name = 'default') {
        this.modals[name] = true;
    }

    close(name = 'default') {
        this.modals[name] = false;
    }

    isOpened(name = 'default') {
        return this.modals[name];
    }
}
