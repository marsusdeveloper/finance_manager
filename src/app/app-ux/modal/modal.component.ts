import {Component, OnInit, Input, HostListener} from '@angular/core';
import {ModalService} from './modal.service';

@Component({
    selector: 'app-ux-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    private escapeKeyCode = 27;

    @Input()
    name = 'default';

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        console.log(event);

        if (event.keyCode === this.escapeKeyCode) {
            this.service.close(this.name);
        }
    }

    constructor(public service: ModalService) {

    }

    ngOnInit() {
    }

    stopPropagationOnClick(event) {
        event.stopPropagation();
    }
}
