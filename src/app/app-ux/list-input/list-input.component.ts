import {Component, Input, forwardRef} from '@angular/core';
import {
    NG_VALUE_ACCESSOR,
    ControlValueAccessor
} from '@angular/forms';

@Component({
    selector: 'app-ux-list-input',
    templateUrl: './list-input.component.html',
    styleUrls: ['list-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ListInputComponent),
            multi: true
        }
    ],
})

export class ListInputComponent implements ControlValueAccessor {
    @Input() options = [];
    value;

    writeValue(_value: any) {
        this.value = _value;
    }

    propagateChange = (_: any) => {};

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched() {
    }

    setValue(_value) {
        this.value = _value;
        this.propagateChange(_value);
    }

}
