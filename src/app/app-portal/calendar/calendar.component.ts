import {Component} from '@angular/core';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {
    monthValue = 1;
    calculateYear = 2017;
    days = [];
    monthName = [
        'styczen',
        'luty',
        'marzec',
        'kwiecien',
        'maj',
        'czerwiec',
        'lipiec',
        'sierpień',
        'wrzesień',
        'październik',
        'listopad',
        'grudzień'
    ];

    dayName = [
        'niedziela',
        'poniedziałek',
        'wtorek',
        'środa',
        'czwartek',
        'piątek',
        'sobota'
    ];

    yearsSelector = {
        'id': 'year',
        'type': 'select',
        'options': []
    };

    monthSelector = {
        'id': 'month',
        'type': 'select',
        'options': []
    };

    private currentDate: Date;
    private yearsBack = 5;

    constructor() {
        this.currentDate = new Date(Date.now());
        this.calculateYear = this.currentDate.getFullYear();
        this.monthValue = this.currentDate.getMonth();
        this.prepareSelectors();
        this.calculateDaysInMonth();
    }

    calculateDaysInMonth() {
        const days = [];
        const date = new Date(this.calculateYear, this.monthValue, 1);
        for (let i = 0; i < date.getDay(); i++) {
            days.push(false);
        }
        while (date.getMonth() === this.monthValue) {
            days.push(new Date(date));
            date.setDate(date.getDate() + 1);
        }
        this.days = days;
    }

    prepareSelectors() {
        for (const index in this.monthName) {
            if (this.monthName[index] === undefined) {
                continue;
            }
            this.monthSelector.options.push({
                label: this.monthName[index],
                value: index
            });
        }

        for (let year = this.calculateYear - this.yearsBack; year <= this.calculateYear + this.yearsBack; year++) {
            this.yearsSelector.options.push({
                label: year,
                value: year
            });
        }
    }

    set year(value) {
        this.calculateYear = value;
        this.calculateDaysInMonth();
    }

    get year() {
        return this.calculateYear;
    }

    set month(value) {
        this.monthValue = Number(value);
        this.calculateDaysInMonth();
    }

    get month() {
        return this.monthValue;
    }

    setMonth(difference) {
        let month = this.month + difference;
        let year = this.year;

        if (month < 0) {
            year = this.year - 1;
            month = 11;
        } else if (month > 11) {
            year = this.year + 1;
            month = 0;
        }

        if (this.yearsSelector.options.findIndex((a) => a.value.toString() === year.toString()) !== -1) {
            this.month = month;
            this.year = year;
        }
    }

    getDayLink(date) {
        if (date) {
            const dateString = `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`;
            return ['/dzien', dateString];
        }
        return [];
    }
}
