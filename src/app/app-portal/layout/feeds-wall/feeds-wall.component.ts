import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-feeds-wall',
  templateUrl: './feeds-wall.component.html',
  styleUrls: ['./feeds-wall.component.scss']
})
export class FeedsWallComponent implements OnInit {
  @Input()
  incomingFeeds = [];

  constructor() { }

  ngOnInit() {
  }

}
