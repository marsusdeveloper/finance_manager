import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppBackendModule} from 'app/app-backend/app-backend.module';
import {AppUxModule} from 'app/app-ux/app-ux.module';
import {RouterModule} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeaderComponent} from './layout/header/header.component';
import {CategoriesComponent} from './home/categories/categories.component';
import {WallComponent} from './home/wall/wall.component';
import {QuickPaymentComponent} from './home/quick-payment/quick-payment.component';
import {ActionsComponent} from './layout/actions/actions.component';
import {CalendarComponent} from './calendar/calendar.component';
import { DayViewComponent } from './day-view/day-view.component';
import {SharedModule} from 'app/shared/shared.module';
import { FeedsWallComponent } from './layout/feeds-wall/feeds-wall.component';
import { AddCategoryComponent } from './home/categories/add-category/add-category.component';
import {CategoriesService} from './home/categories/categories.service';

@NgModule({
    imports: [
        CommonModule,
        AppBackendModule,
        AppUxModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        SharedModule
    ],
    declarations: [
        NotFoundComponent,
        HomeComponent,
        HeaderComponent,
        CategoriesComponent,
        WallComponent,
        QuickPaymentComponent,
        ActionsComponent,
        CalendarComponent,
        DayViewComponent,
        FeedsWallComponent,
        AddCategoryComponent,
    ],
    exports: [
        HeaderComponent
    ],
    providers: [
        CategoriesService
    ]
})
export class AppPortalModule {
}
