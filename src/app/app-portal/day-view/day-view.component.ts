import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-day-view',
    templateUrl: './day-view.component.html',
    styleUrls: ['./day-view.component.scss']
})
export class DayViewComponent implements OnInit {
    date: Date;
    incomingFeeds = [
        {
            category: {
                name: 'Zakupy',
                color: 'red'
            },
            name: 'Testowy alert',
            date: '01.10.2017 14:20',
            isAlert: true
        },
        {
            category: {
                name: 'Zakupy',
                color: 'blue'
            },
            name: 'Rata za samochod',
            date: '02.10.2017 14:20',
            isAlert: false
        }
    ];

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.date = new Date(params['date']);
        });
    }

}
