import {Component} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {
    header = {
        'index': {
            fieldName: 'Identyfikator',
            callback: a => a + 1
        },
        'isActive': {
            fieldName: 'Czy konto jest aktywne',
            callback: a => a ? 'Tak' : 'Nie'
        },
        'registered': {
            fieldName: 'Data rejestracji'
        }
    };
    formList = [
        {
            id: 'club_id',
            title: 'Klub',
            type: 'autocomplete',
        },
        {
            id: 'email',
            title: 'Email',
            type: 'text',
        }
    ];
    data = [
        {
            '_id': '59df618bc80a7d92f9d26f8c',
            'index': 0,
            'guid': '471e4140-0487-4b6d-b3a6-b1f55c8dbd61',
            'isActive': true,
            'balance': '$3,167.83',
            'picture': 'http://placehold.it/32x32',
            'age': 37,
            'eyeColor': 'brown',
            'name': 'Kim Hensley',
            'gender': 'female',
            'company': 'PHOLIO',
            'email': 'kimhensley@pholio.com',
            'phone': '+1 (843) 513-2380',
            'address': '436 Broadway , Dodge, Alaska, 3803',
            'registered': '2015-05-11T09:20:29 -02:00',
            'latitude': 10.737684,
            'longitude': -92.644667,
            'tags': [
                'quis',
                'Lorem',
                'ea',
                'excepteur',
                'officia',
                'cillum',
                'esse'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Levine Sheppard'
                },
                {
                    'id': 1,
                    'name': 'Katie Baker'
                },
                {
                    'id': 2,
                    'name': 'Nieves Dunn'
                }
            ],
            'greeting': 'Hello, Kim Hensley! You have 7 unread messages.',
            'favoriteFruit': 'strawberry'
        },
        {
            '_id': '59df618b13ca280f84de0a13',
            'index': 1,
            'guid': 'ac2dcac8-058a-4dc9-bae0-305cd4c634d4',
            'isActive': false,
            'balance': '$2,217.27',
            'picture': 'http://placehold.it/32x32',
            'age': 28,
            'eyeColor': 'green',
            'name': 'Amy Ramsey',
            'gender': 'female',
            'company': 'TALAE',
            'email': 'amyramsey@talae.com',
            'phone': '+1 (858) 573-2329',
            'address': '292 Irving Street, Wells, Colorado, 1609',
            'registered': '2017-01-02T02:05:25 -01:00',
            'latitude': 11.435069,
            'longitude': -14.201207,
            'tags': [
                'ad',
                'fugiat',
                'nulla',
                'Lorem',
                'adipisicing',
                'Lorem',
                'dolor'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Lula Cortez'
                },
                {
                    'id': 1,
                    'name': 'Molina Underwood'
                },
                {
                    'id': 2,
                    'name': 'Mercer Cochran'
                }
            ],
            'greeting': 'Hello, Amy Ramsey! You have 9 unread messages.',
            'favoriteFruit': 'apple'
        },
        {
            '_id': '59df618b442666763b70ef31',
            'index': 2,
            'guid': '85050e3e-1c2d-4125-b495-07d6691907bb',
            'isActive': true,
            'balance': '$1,651.10',
            'picture': 'http://placehold.it/32x32',
            'age': 38,
            'eyeColor': 'brown',
            'name': 'Alice Estrada',
            'gender': 'female',
            'company': 'ZAPPIX',
            'email': 'aliceestrada@zappix.com',
            'phone': '+1 (980) 538-2326',
            'address': '768 Lincoln Terrace, Newkirk, Tennessee, 6365',
            'registered': '2014-11-20T05:15:52 -01:00',
            'latitude': 18.040155,
            'longitude': 148.663022,
            'tags': [
                'deserunt',
                'qui',
                'sit',
                'nulla',
                'do',
                'amet',
                'pariatur'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Jeannie Benjamin'
                },
                {
                    'id': 1,
                    'name': 'Monique Warner'
                },
                {
                    'id': 2,
                    'name': 'Mercedes Jenkins'
                }
            ],
            'greeting': 'Hello, Alice Estrada! You have 5 unread messages.',
            'favoriteFruit': 'apple'
        },
        {
            '_id': '59df618b3e135bf3a13e1023',
            'index': 3,
            'guid': '1c231c4a-7854-4374-ba30-3c140b511170',
            'isActive': false,
            'balance': '$1,973.31',
            'picture': 'http://placehold.it/32x32',
            'age': 35,
            'eyeColor': 'blue',
            'name': 'Darcy Briggs',
            'gender': 'female',
            'company': 'ZORROMOP',
            'email': 'darcybriggs@zorromop.com',
            'phone': '+1 (976) 472-3629',
            'address': '281 Rapelye Street, Monument, Iowa, 9877',
            'registered': '2016-02-01T07:04:52 -01:00',
            'latitude': 34.676207,
            'longitude': 68.648026,
            'tags': [
                'sint',
                'aute',
                'amet',
                'officia',
                'dolor',
                'ipsum',
                'minim'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Mendoza Brock'
                },
                {
                    'id': 1,
                    'name': 'Logan Burt'
                },
                {
                    'id': 2,
                    'name': 'Kay Floyd'
                }
            ],
            'greeting': 'Hello, Darcy Briggs! You have 6 unread messages.',
            'favoriteFruit': 'banana'
        },
        {
            '_id': '59df618bbcf85dd973c5b8c5',
            'index': 4,
            'guid': '0ec1b04e-7e4f-4c6c-9f91-e9ab1bfdc90a',
            'isActive': false,
            'balance': '$1,538.79',
            'picture': 'http://placehold.it/32x32',
            'age': 32,
            'eyeColor': 'blue',
            'name': 'Faye Kane',
            'gender': 'female',
            'company': 'ROUGHIES',
            'email': 'fayekane@roughies.com',
            'phone': '+1 (908) 521-3421',
            'address': '344 Highland Boulevard, Evergreen, Guam, 9010',
            'registered': '2016-02-12T10:30:45 -01:00',
            'latitude': -30.538531,
            'longitude': -97.324511,
            'tags': [
                'commodo',
                'anim',
                'laborum',
                'ullamco',
                'adipisicing',
                'Lorem',
                'reprehenderit'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Nora Guerra'
                },
                {
                    'id': 1,
                    'name': 'Burke Edwards'
                },
                {
                    'id': 2,
                    'name': 'Claudette Pate'
                }
            ],
            'greeting': 'Hello, Faye Kane! You have 8 unread messages.',
            'favoriteFruit': 'strawberry'
        },
        {
            '_id': '59df618b2dd402ce558ba5dc',
            'index': 5,
            'guid': '10f28601-fc66-4088-855a-562701730cf0',
            'isActive': false,
            'balance': '$3,544.50',
            'picture': 'http://placehold.it/32x32',
            'age': 26,
            'eyeColor': 'green',
            'name': 'Rosalyn Savage',
            'gender': 'female',
            'company': 'ENDIPIN',
            'email': 'rosalynsavage@endipin.com',
            'phone': '+1 (831) 568-2775',
            'address': '804 Nassau Avenue, Catherine, Virgin Islands, 2716',
            'registered': '2014-09-29T04:59:38 -02:00',
            'latitude': 23.572156,
            'longitude': 53.499217,
            'tags': [
                'sit',
                'aliquip',
                'ullamco',
                'cillum',
                'occaecat',
                'mollit',
                'occaecat'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Patrice Decker'
                },
                {
                    'id': 1,
                    'name': 'Lynda Rosario'
                },
                {
                    'id': 2,
                    'name': 'Sampson Hammond'
                }
            ],
            'greeting': 'Hello, Rosalyn Savage! You have 7 unread messages.',
            'favoriteFruit': 'banana'
        },
        {
            '_id': '59df618bf869fe8b6510f240',
            'index': 6,
            'guid': '32d70eb0-72c8-43ec-b480-d51259d011ab',
            'isActive': true,
            'balance': '$3,661.06',
            'picture': 'http://placehold.it/32x32',
            'age': 36,
            'eyeColor': 'blue',
            'name': 'Church Lindsey',
            'gender': 'male',
            'company': 'EXIAND',
            'email': 'churchlindsey@exiand.com',
            'phone': '+1 (876) 497-2689',
            'address': '218 Stockholm Street, Garnet, Illinois, 1594',
            'registered': '2014-04-15T02:53:01 -02:00',
            'latitude': -48.853695,
            'longitude': 168.418292,
            'tags': [
                'reprehenderit',
                'ut',
                'esse',
                'cillum',
                'occaecat',
                'laborum',
                'adipisicing'
            ],
            'friends': [
                {
                    'id': 0,
                    'name': 'Flora Ochoa'
                },
                {
                    'id': 1,
                    'name': 'Janelle Boyd'
                },
                {
                    'id': 2,
                    'name': 'Moreno Roman'
                }
            ],
            'greeting': 'Hello, Church Lindsey! You have 10 unread messages.',
            'favoriteFruit': 'strawberry'
        }
    ];

    addPayment() {
        console.log('payment');
    }
}
