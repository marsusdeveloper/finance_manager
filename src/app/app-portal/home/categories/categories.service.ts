import {Injectable} from '@angular/core';

interface Category {
    color: string;
    name: string;
}


@Injectable()
export class CategoriesService {
    private categories: Category[] = [
        {
            color: 'red',
            name: 'Zakupy'
        },
        {
            color: 'blue',
            name: 'Domowe'
        },
    ];

    constructor() {
    }

    addNew(category: Category) {
        this.categories.push(category);
    }

    getCategories() {
        return this.categories;
    }
}
