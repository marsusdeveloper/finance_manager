import {Component, OnInit} from '@angular/core';
import {ModalService} from 'app/app-ux/modal/modal.service';
import {CategoriesService} from './categories.service';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
    modalName = 'categoriesModal';

    constructor(public modalService: ModalService, public categoriesService: CategoriesService) {

    }

    ngOnInit() {
    }

    addCategory() {
        this.modalService.open(this.modalName);
    }
}
