import {Component, OnInit} from '@angular/core';
import {CategoriesService} from '../categories.service';

@Component({
    selector: 'app-add-category',
    templateUrl: './add-category.component.html',
    styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
    form = [
        {
            id: 'name',
            title: 'Nazwa kategorii',
            type: 'text'
        },
        {
            id: 'color',
            title: 'Kolor kategorii',
            type: 'text'
        }
    ];

    constructor(private categoriesService: CategoriesService) {
    }

    ngOnInit() {
    }

    submitForm($event) {
        if ($event.valid) {
            this.categoriesService.addNew($event.value);
        }
    }

}
