import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-wall',
    templateUrl: './wall.component.html',
    styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit {
    incomingFeeds = [
        {
            category: {
                name: 'Zakupy',
                color: 'red'
            },
            name: 'Testowy alert',
            date: '01.10.2017 14:20',
            isAlert: true
        },
        {
            category: {
                name: 'Zakupy',
                color: 'blue'
            },
            name: 'Rata za samochod',
            date: '02.10.2017 14:20',
            isAlert: false
        }
    ];

    ngOnInit() {
    }
}
