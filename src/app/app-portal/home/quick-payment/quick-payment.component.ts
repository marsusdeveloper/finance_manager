import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-quick-payment',
    templateUrl: './quick-payment.component.html',
    styleUrls: ['./quick-payment.component.scss']
})
export class QuickPaymentComponent implements OnInit {
    payments = [
        {
            name: 'ZUS',
            cost: 20
        }
    ];

    constructor() {
    }

    ngOnInit() {
    }

    addQuickPayment() {
        this.payments.push({
           name: 'Test',
           cost: 10
        });
    }
}
