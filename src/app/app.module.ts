import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppPortalModule} from 'app/app-portal/app-portal.module';
import {SharedModule} from 'app/shared/shared.module';
import {appRoutes} from './routes';
import {AppComponent} from './app.component';
import {AppUxModule} from './app-ux/app-ux.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        HttpModule,
        AppUxModule,
        AppPortalModule,
        SharedModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
