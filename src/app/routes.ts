import {Routes} from '@angular/router';

import {HomeComponent} from './app-portal/home/home.component';
import {NotFoundComponent} from './app-portal/not-found/not-found.component';
import {CalendarComponent} from './app-portal/calendar/calendar.component';
import {DayViewComponent} from './app-portal/day-view/day-view.component';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'kalendarz', component: CalendarComponent},
    {path: 'dzien/:date', component: DayViewComponent},
    {path: '**', component: NotFoundComponent}
];
