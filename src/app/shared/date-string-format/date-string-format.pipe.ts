import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'dateStringFormat'
})
export class DateStringFormatPipe implements PipeTransform {

    transform(value: Date, format = 'mm.dd.yyyy'): string {
        return format.replace('yyyy', value.getFullYear().toString())
            .replace('mm', this.parseNumberToString(value.getMonth() + 1))
            .replace('dd', this.parseNumberToString(value.getDate()));
    }

    private parseNumberToString(value) {
        return (value > 9 ? value : `0${value}`).toString();
    }
}
