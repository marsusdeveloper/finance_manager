import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateStringFormatPipe } from './date-string-format/date-string-format.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DateStringFormatPipe],
    exports: [
        DateStringFormatPipe
    ],
    providers: [
        DateStringFormatPipe
    ]
})
export class SharedModule { }
